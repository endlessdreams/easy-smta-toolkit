FAO SMTA API post submiter
==========================

**Intended use for Gene banks**

Easysmta is a http post utillity program to register your institute's SMTAs (Standard Material Transfer Agreements). To make it work, you need to have the following:

1. An account at [FAO's Planttreaty site](https://mls.planttreaty.org/itt/).
2. Two database tables or dataviews with SMTA and SMTA material data. 
3. Make the configuration in this application's config file.
4. run bin/easysmta post command. The command uses parameters to limit which SMTAs you are intended to post/register.




## Installation

In order to install this library via composer run the following command in the console:

```sh
composer require endlessdreams/easy-smta-toolkit
```

or add the package manually to your composer.json file in the require section:

```json
"endlessdreams/easy-smta-toolkit": "^1.0"
```

or globally:

```sh
composer global require endlessdreams/easy-smta-toolkit
```

This will install easysmta into the ~/.composer/vendor/ directory and, most importantly, the easysmta CLI tool are installed into ~/.composer/vendor/bin/.

Simply add this directory to your PATH in your ~/.bash_profile (or ~/.bashrc) like this:

```sh
export PATH=~/.composer/vendor/bin:$PATH
```

and easysmta is now available on your command line.

To keep your tools up to date, you simply do this:

```sh
composer global update
composer global install
```

To remove a package, you edit ~/.composer/composer.json and then run composer global update.


## Configuration

Configuration file is located in easysmta application folder: config/local.php.
In this file you will see the following structure:


```php

return [
    'database' => [
        'driver' => getenv('EASYSMTA_DRIVER'),
        'database' => getenv('EASYSMTA_DATABASE'),
        'username' => getenv('EASYSMTA_USERNAME'),
        'password' => getenv('EASYSMTA_PASSWORD'),
        'hostname'  => getenv('EASYSMTA_HOSTNAME'),
    ],
    'map' => [
        'table_order' => 'easysmta',
        'table_item' => 'easysmtaitem',
        'columns_order' => [
            'id' => 'id',
            'symbole' => 'symbole',
            'date' => 'date',
            'type' => 'type',
            'language' => 'language',
            'shipName' => 'shipname',
            'recipient_type' => 'recipient_type',
            'recipient_pid' => 'recipient_pid',
            'recipient_name' => 'recipient_name',
            'recipient_address' => 'recipient_address',
            'recipient_country' => 'recipient_country',
            'document_location' => 'document_location',
            'document_retInfo' => 'document_retinfo'
        ],
        'columns_item' => [
            'crop' => 'crop',
            'sampleid' => 'sampleid',
            'pud' => 'pud',
            'ancestry' => 'ancestry'
        ],
    ],
    'provider' => [
        'type' => 'or',
        'pid' => getenv('PROVIDER_INSTITUTE_CODE') ?? 'YOUR INSTITUTE CODE',
        'name' => getenv('PROVIDER_INSTITUTE_NAME') ?? 'YOUR INSTITUTE NAME',
        'address' => getenv('PROVIDER_INSTITUTE_ADDRESS') ?? 'YOUR INSTITUTE ADDRESS',
        'country' => getenv('PROVIDER_INSTITUTE_COUNTRY_CODE') ?? 'XXX',
        'email' => getenv('PROVIDER_INSTITUTE_EMAIL') ?? 'contact@any.institute'
    ],
    'fao' => [
        'username' => getenv('EASYSMTA_FAO_USERNAME'),
        'password' => getenv('EASYSMTA_FAO_PASSWORD'),
        'testusername' => getenv('EASYSMTA_FAO_TEST_USERNAME'),
        'testpassword' => getenv('EASYSMTA_FAO_TEST_PASSWORD'),
    ],
];

```


To set database and FAO HTTPS API credentials in the config file or exporting environment variables in ~/.bash_profile (or ~/.bashrc).


## Usage

The examples are written as if your working directory is bin or the application is globally installed.

To test generate xml to terminal.

```sh
easysmta xml-dry-run
```

To test generate all smta xml to 2018-01-01. (No https credentials needed here, only to your database)

```sh
easysmta xml-dry-run --to=2018-01-01
```

To test post all SMTA's until 2018-01-01.

```sh
easysmta register --to=2018-01-01 -t
```

To test post all SMTA's from 2017-01-01 until 2018-01-01.

```sh
easysmta register --from=2017-01-01 --to=2018-01-01 -t
```


To actual post all SMTA's until 2018-01-01. (No testing)


```sh
easysmta register --to=2018-01-01
```


Credits
-------


ir. R (Roel) Hoekstra and dr.ir. TJL (Theo) van Hintum
at Centrum voor Genetische Bronnen Nederland
for letting me look at their dos cmd toolkit.

Mr Marco Marsella at FAO, Climate, Biodiversity, Land and Water Department, IT for great support.