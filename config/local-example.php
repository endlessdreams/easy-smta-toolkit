<?php
return [
    'database' => [
        'driver' => getenv('EASYSMTA_DRIVER'),
        'database' => getenv('EASYSMTA_DATABASE'),
        'username' => getenv('EASYSMTA_USERNAME'),
        'password' => getenv('EASYSMTA_PASSWORD'),
        'hostname'  => getenv('EASYSMTA_HOSTNAME'),
    ],
    'map' => [
        'table_order' => 'easysmta',
        'table_item' => 'easysmtaitem',
        'columns_order' => [
            'id' => 'id',
            'symbole' => 'symbole',
            'date' => 'date',
            'type' => 'type',
            'language' => 'language',
            'shipName' => 'shipname',
            'recipient_type' => 'recipient_type',
            'recipient_pid' => 'recipient_pid',
            'recipient_name' => 'recipient_name',
            'recipient_address' => 'recipient_address',
            'recipient_country' => 'recipient_country',
            'document_location' => 'document_location',
            'document_retInfo' => 'document_retinfo'
        ],
        'columns_item' => [
            'crop' => 'crop',
            'sampleid' => 'sampleid',
            'pud' => 'pud',
            'ancestry' => 'ancestry'
        ],
    ],
    'provider' => [
        'type' => 'or',
        'pid' => getenv('PROVIDER_INSTITUTE_CODE') ?? 'YOUR INSTITUTE CODE',
        'name' => getenv('PROVIDER_INSTITUTE_NAME') ?? 'YOUR INSTITUTE NAME',
        'address' => getenv('PROVIDER_INSTITUTE_ADDRESS') ?? 'YOUR INSTITUTE ADDRESS',
        'country' => getenv('PROVIDER_INSTITUTE_COUNTRY_CODE') ?? 'XXX',
        'email' => getenv('PROVIDER_INSTITUTE_EMAIL') ?? 'contact@any.institute'
    ],
    'fao' => [
        'username' => getenv('EASYSMTA_FAO_USERNAME'),
        'password' => getenv('EASYSMTA_FAO_PASSWORD'),
        'testusername' => getenv('EASYSMTA_FAO_TEST_USERNAME'),
        'testpassword' => getenv('EASYSMTA_FAO_TEST_PASSWORD'),
    ],
];