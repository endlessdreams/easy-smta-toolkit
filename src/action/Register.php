<?php
/**
 * FAO Easy SMTA Api Toolkit
 * Copyright (C) 2018 Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018 Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/easy-smta-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 */

namespace endlessdreams\easySmtaToolkit\action;

use ZF\Console\Route;
use Zend\Config\Config;
use Zend\Console\Adapter\AdapterInterface;
use Zend\Db\Adapter\Adapter;
use endlessdreams\easySmtaToolkit\helper\XmlGenerator;
use Curl\Curl;



/**
 * @author Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @since v0.0.1
 */
class Register extends XmlDryRun {
    
    
    /** @var array */
    protected $fao;

    /**
     *
     */
    public function __construct() {
        parent::__construct();
        
        if (is_file(__DIR__.'/../../config/local.php')) {
            copy(__DIR__.'/../../config/local-example.php', __DIR__.'/../../config/local.php');
        }
        $config = new Config(include __DIR__.'/../../config/local.php');
        $this->fao = $config->fao->toArray();
    }


    /**
     * @param \ZF\Console\Route $route
     * @param \Zend\Console\Adapter\AdapterInterface $console
     * @throws \Exception
     */
    public function __invoke(Route $route, AdapterInterface $console)
    {
        if (isset($this->db) && $this->db instanceof Adapter) {
            try {
                $xml = new XmlGenerator($this->db, $route);
                $t = $route->getMatchedParam('t');
                foreach ($this->getSmtaRecords($route) as $r) {
                    $xml->row = $r;
                    $doCompress = ($xml->getResults()->count()>=100);
                    $post_data = array(
                        'username' => ($t != true) ? $this->fao['username'] : $this->fao['testusername'],
                        'password' => ($t != true) ? $this->fao['password'] : $this->fao['testpassword'],
                        'compressed' => ($doCompress ? 'y' : 'n'),
                        'xml'=> ($doCompress ? gzencode($xml(), 9) : $xml())
                    );
                    $url = ($t == true) 
                        ? "https://easy-smta-test.planttreaty.org/itt/index.php?r=extsys/uploadxml"
                        : "https://mls.planttreaty.org/itt/index.php?r=extsys/uploadxml";
                    $port=443;
                    
                    $console->writeLine($this->_post($post_data,$url,$port));
                    
                }
            } catch (\Exception $e) {
                $console->writeLine("SMTA {$r['symbole']}: $e");
            }
        } else {
            throw new \Exception("There is no config file. Make sure to create one. A default config file can be crated with command action 'create-default-config.'");
        }
    }
    
    
    
    
    /**
     * @param array $post_xml
     * @param string $url
     * @param int $port
     * @throws \Exception
     * @return string
     */
    protected function _post($post_xml,$url,$port) {
        /** @var Curl */
        $curl = new Curl();
                
        $curl->setOpt(CURLOPT_FAILONERROR, TRUE); // Fail on errors
        $curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE); // return into a variable
        $curl->setOpt(CURLOPT_PORT, $port); //Set the port number
        $curl->setOpt(CURLOPT_TIMEOUT, 1800); // times out after 30 minutes
        $curl->setOpt(CURLOPT_HTTPHEADER, array('Connection: close'));
        $curl->setOpt(CURLOPT_HEADER, TRUE); // Show header in reply

        
        if($port==443) {
            //curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
            $curl->setOpt(CURLOPT_SSL_VERIFYHOST, 2);
            //curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,1);
            $curl->setOpt(CURLOPT_SSL_VERIFYPEER, 1);
        }
        
        $curl->post($url, $post_xml);
        
        if ($curl->isError()) {
            // return 'ERROR -> '.curl_errno($ch).': '.curl_error($ch);
            return 'ERROR -> '.$curl->curl_error_code.': '.$curl->curl_error_message;
        } else {
            switch($curl->http_status_code){
                case 200:
                    break;
                default:
                    throw new \Exception('HTTP ERROR -> '.$curl->http_status_code);
                    break;
            }
        }
        $data = $curl->response;
        $curl->close();
        
        return $data;
    }

}