<?php
/**
 * FAO Easy SMTA Api Toolkit
 * Copyright (C) 2018 Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018 Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/easy-smta-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 */

namespace endlessdreams\easySmtaToolkit\action;

use Zend\Config\Config;
use Zend\Db\Adapter\Adapter;
use endlessdreams\easySmtaToolkit\helper\XmlGenerator;
use ZF\Console\Route;
use Zend\Console\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;




/**
 * @author Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @since v0.0.1
 */
class XmlDryRun {
    
    /** @var Adapter */
    protected $db;
    
    /** @var array */
    protected $dbMap;
    
    /** @var array */
    protected $provider;
    
    
    /**
     * 
     */
    public function __construct() {
        if (!is_file(__DIR__.'/../../config/local.php')) {
            copy(__DIR__.'/../../config/local-example.php', __DIR__.'/../../config/local.php');
        }
        $config = new Config(include __DIR__.'/../../config/local.php');
        $this->db = new Adapter($config->database->toArray());
        
        $this->dbMap = $config->map->toArray();
        $this->provider = $config->provider->toArray();
    }
    
    
    
    /**
     * @param \ZF\Console\Route $route
     * @param \Zend\Console\Adapter\AdapterInterface $console
     */
    public function __invoke(Route $route, AdapterInterface $console)
    {
        $xml = new XmlGenerator($this->db, $route);
        
        $results = $this->getSmtaRecords($route);
        
        foreach ($results as $r) {
            $xml->row = $r;
            $console->writeLine( $xml() );
        }
    }
    
    /**
     * @param \ZF\Console\Route $route
     * @return \Zend\Db\ResultSet\ResultSet
     */
    protected function getSmtaRecords(Route $route) {
        $from = $route->getMatchedParam('from');
        $to = $route->getMatchedParam('to');
        
        $sql = "SELECT * FROM " . $this->dbMap['table_order'] . " where true";
        if(isset($from)) {
            $sql .= " and date > '$from'";
        }
        
        if(isset($to)) {
            $sql .= " and date < '$to'";
        }
        
        $statement = $this->db->query($sql);

        /** @var $results \Zend\Db\Adapter\Driver\ResultInterface */
        $results = $statement -> execute();

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        $resultSet->buffer();

        return $resultSet;
    }
}