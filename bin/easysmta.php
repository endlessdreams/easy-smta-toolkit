#!/usr/bin/env php
<?php 
/**
 * FAO Easy SMTA Api Toolkit
 * Copyright (C) 2018 Endless-Dream(R), Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * @author    Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @copyright 2018 Endless-Dreams(R)
 * @license   https://bitbucket.org/endlessdreams/easy-smta-toolkit/src/master/LICENSE.md AGPL-3.0 Licence
 */
$vendorroot = explode('/vendor',__DIR__);

if (count($vendorroot) > 1 && is_file( $vendorroot[0].'/vendor/autoload.php') === true) {
    include_once $vendorroot[0] .'/vendor/autoload.php'; // This library is included in another project
} else {
    include_once dirname($vendorroot[0]).'/vendor/autoload.php'; // This library is runed stand alone
}

// Check for php7
if (!defined('PHP_MAJOR_VERSION') || PHP_MAJOR_VERSION < 7) {
    die(
        'Upgrade to php7' . PHP_EOL .
        'Easy SMTA Api Toolkit upports only php7 and above.' . PHP_EOL 
        );
}


use ZF\Console\Application;
use ZF\Console\Dispatcher;
use Zend\Console\ColorInterface;
use Zend\Console\Console;
use endlessdreams\easySmtaToolkit\action\Register;
use endlessdreams\easySmtaToolkit\action\XmlDryRun;


define('VERSION', '0.0.4');

$dispatcher = new Dispatcher();
$dispatcher->map('xml-dry-run', new XmlDryRun());
$dispatcher->map('register', new Register());


$application = new Application(
    'easysmta',
    VERSION,
    include __DIR__ . '/../config/routes.php',
    Console::getInstance(),
    $dispatcher
    );
$application->setBanner(function (\Zend\Console\Adapter\AdapterInterface $console) {           // callable
    $console->writeLine(
    $console->colorize('easysmta', ColorInterface::BLUE)
    . ' - for utilize Easy SMTA data API of FAO'
        );
    $console->writeLine('');
    $console->writeLine('Usage:', ColorInterface::GREEN);
    $console->writeLine(' ' . basename(__FILE__) . ' command [options]');
    $console->writeLine('');
    $console->writeLine('Copyright 2018 Endless-Dreams');
});
    
    $application->setBannerDisabledForUserCommands(true);
    $application->setFooter(null);
    $exit = $application->run();
    exit($exit);
    